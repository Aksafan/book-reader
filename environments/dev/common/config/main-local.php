<?php
return [
    'components' => [
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'pgsql:host=localhost;dbname=book_reader',
            'username' => 'root',
            'password' => '',
            'charset' => 'utf8',
        ],
    ],
];
